@echo off 

rem https://forums.embarcadero.com/thread.jspa?threadID=90959
if /i NOT "%PLATFORM%" == "AnyCPU" set  PLATFORM=AnyCPU
echo Platform varible: %PLATFORM%

setlocal
set msbuild="C:\Windows\Microsoft.NET\Framework64\v4.0.30319\MSBuild.exe"
rem set target=/t:LoadNuGetPackages
set target=/t:CodeCoverageReport
set projectfolder=RepositoryGenerator.Domain

set opts=/nologo /m /v:m 
set script=_ci.msbuild
                            
%msbuild% %script% %target% /p:projectfolder=%projectfolder%
   
 
endlocal

