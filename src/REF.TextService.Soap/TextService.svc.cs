﻿using REF.TextService.Contracts.DTOs.V1;
using REF.TextService.Domain.Contracts;
using REF.TextService.Domain.Processors;
using System.Collections.Generic;

namespace REF.TextService.Soap
{

    public class TextService : ITextService
    {
        private readonly ITextSorter _textSorter;
        private readonly ITextAnalyzer _textAnayzer;

        /// <summary>
        /// Initializes a new instance of the <see cref="TextService"/> class.
        /// </summary>
        /// <param name="sorter">The sorter.</param>
        /// <param name="analyzer">The analyzer.</param>
        public TextService(ITextSorter sorter, ITextAnalyzer analyzer)
        {
            _textSorter = sorter;
            _textAnayzer = analyzer;
        }

        /// <summary>
        /// Sorts the specified text.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="option">The option.</param>
        /// <returns></returns>
        public IEnumerable<string> Sort(string text, SortOption option)
        {
            return _textSorter.Sort(text, option);
        }

        /// <summary>
        /// Generates the statistics.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        public TextStatistics GenerateStatistics(string text)
        {
            return _textAnayzer.Analyse(text);
        }
    }


}
