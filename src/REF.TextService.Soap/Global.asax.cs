﻿using Ninject;
using Ninject.Web.Common;
using REF.TextService.Soap.Code;
using Bootstrapper = REF.TextService.Soap.Code.Bootstrapper;

namespace REF.TextService.Soap
{
    public class Global : NinjectHttpApplication
    {
        protected override IKernel CreateKernel()
        {
            var bootstrapper = new Bootstrapper();
            return (IKernel)bootstrapper.Bootup(ObjectLifeStyle.InRequestScope);

        }
    }
}