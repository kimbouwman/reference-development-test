﻿using Ninject;

namespace REF.TextService.Soap.Code
{
    public class Bootstrapper
    {
        public object Bootup(ObjectLifeStyle objectLifeStyle)
        {
            var kernel = new StandardKernel(new TextServiceDomainModule(objectLifeStyle));
            return kernel;
        }
    }
}