﻿using Ninject.Extensions.Conventions;
using Ninject.Modules;
using Ninject.Syntax;
using Ninject.Web.Common;

namespace REF.TextService.Soap.Code
{
    public class TextServiceDomainModule : NinjectModule
    {
        readonly ObjectLifeStyle _objectLifeStyle;

        public TextServiceDomainModule(ObjectLifeStyle lifeStyle)
        {
            _objectLifeStyle = lifeStyle;
        }

        public override void Load()
        {
            Kernel.Bind(x => x
                .From("REF.TextService.Domain")
                .SelectAllClasses()
                .BindAllInterfaces()
                .Configure(y => ConfigureBinding(y)));
        }

        private IBindingNamedWithOrOnSyntax<object> ConfigureBinding(IBindingWhenInNamedWithOrOnSyntax<object> b)
        {
            switch (_objectLifeStyle)
            {
                case ObjectLifeStyle.InRequestScope: return b.InRequestScope();
                case ObjectLifeStyle.Singlton: return b.InSingletonScope();
                case ObjectLifeStyle.ThreadScope: return b.InThreadScope();
                default: return b.InTransientScope();
            }
        }

    }

}
