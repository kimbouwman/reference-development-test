
namespace REF.TextService.Soap.Code
{
    public enum ObjectLifeStyle
    {
        Transient = 0,
        Singlton,
        ThreadScope,
        InRequestScope,
    }
}
