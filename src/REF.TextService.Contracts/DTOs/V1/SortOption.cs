using System.Runtime.Serialization;

namespace REF.TextService.Contracts.DTOs.V1
{
    [DataContract(Namespace = "REF.TextService.Contracts.DTOs.V1")]
    public enum SortOption
    {
        [EnumMember]
        None = 0,

        [EnumMember]
        AscendingLinqSorter,

        [EnumMember]
        DescendingLinqSorter,
    }
}
