using System.Runtime.Serialization;

namespace REF.TextService.Contracts.DTOs.V1
{
    [DataContract(Namespace = "REF.TextService.Contracts.DTOs.V1")]
    public class TextStatistics
    {
        [DataMember]
        public int TotalWords { get; set; }

        [DataMember]
        public int TotalHypens { get; set; }

        [DataMember]
        public int TotalSpaces { get; set; }

    }

}
