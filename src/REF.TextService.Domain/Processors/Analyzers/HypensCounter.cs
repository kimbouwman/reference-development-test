using REF.TextService.Contracts.DTOs.V1;
using REF.TextService.Domain.Contracts;
using System.Linq;

namespace REF.TextService.Domain.Processors.Analyzers
{
    public class HypensCounter : ITextCounter
    {
        public TextStatistics GetTotal(TextStatistics stats, string text)
        {
            stats.TotalHypens = text.Count(TypeDef.HypensPattern.Contains);
            return stats;
        }
    }
}
