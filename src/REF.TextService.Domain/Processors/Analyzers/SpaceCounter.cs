using REF.TextService.Domain.Contracts;
using System.Linq;
using REF.TextService.Contracts.DTOs.V1;

namespace REF.TextService.Domain.Processors.Analyzers
{
    public class SpaceCounter : ITextCounter
    {
        public TextStatistics GetTotal(TextStatistics stats, string text)
        {
            stats.TotalSpaces = text.Count(q => q == ' ');
            return stats;
        }
    }
}
