using REF.TextService.Contracts.DTOs.V1;
using REF.TextService.Domain.Contracts;
using System.Linq;
using System.Text.RegularExpressions;

namespace REF.TextService.Domain.Processors.Analyzers
{
    public class WordCounter : ITextCounter
    {
        public TextStatistics GetTotal(TextStatistics stats, string text)
        {
            var words = Regex.Split(text, TypeDef.WordsPattern).Where(q => !string.IsNullOrEmpty(q));

            stats.TotalWords = words.Count();

            return stats;
        }
    }
}
