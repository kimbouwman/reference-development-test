﻿using REF.TextService.Contracts.DTOs.V1;
using REF.TextService.Domain.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace REF.TextService.Domain.Processors
{
    public interface ITextAnalyzer
    {
        TextStatistics Analyse(string text);
    }

    public class TextAnalyzer : ITextAnalyzer
    {
        readonly IEnumerable<ITextCounter> _counters;

        public TextAnalyzer(IEnumerable<ITextCounter> counters)
        {
            _counters = counters;
        }

        public TextStatistics Analyse(string text)
        {
            var stats = new TextStatistics();

            _counters.ToList().ForEach(c => stats = c.GetTotal(stats, text));
            return stats;
        }
    }

}