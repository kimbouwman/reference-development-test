﻿using REF.TextService.Contracts.DTOs.V1;
using REF.TextService.Domain.Contracts;
using REF.TextService.Domain.Processors.Sorters;
using System.Linq;
using System.Text.RegularExpressions;

namespace REF.TextService.Domain.Processors
{
    public interface ITextSorter
    {
        string[] Sort(string text, SortOption options);
    }

    public class TextSorter : ITextSorter
    {
        readonly ISortBehaviorFactory _sortBehaviorFactory;

        public TextSorter(ISortBehaviorFactory sortBehaviorFactory)
        {
            _sortBehaviorFactory = sortBehaviorFactory;
        }

        public string[] Sort(string text, SortOption option)
        {
            var sortBehavior = _sortBehaviorFactory.Create(option);

            var elements = Regex.Split(text, TypeDef.WordsPattern).Where(q => !string.IsNullOrEmpty(q));
            var result = sortBehavior.Sort(elements);
            return result;
        }
    }
}
