using REF.TextService.Domain.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace REF.TextService.Domain.Processors.Sorters
{
    public class AscendingLinqSorter : ISortBehavior
    {
        public string[] Sort(IEnumerable<string> words)
        {
            return words.OrderBy(q => q).ToArray();
        }
    }
}
