using REF.TextService.Domain.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace REF.TextService.Domain.Processors.Sorters
{
    public class NonSorter : ISortBehavior
    {
        public string[] Sort(IEnumerable<string> words)
        {
            return words.ToArray();
        }
    }
}
