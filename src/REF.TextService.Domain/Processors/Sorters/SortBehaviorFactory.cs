using REF.TextService.Contracts.DTOs.V1;
using REF.TextService.Domain.Contracts;

namespace REF.TextService.Domain.Processors.Sorters
{
    public interface ISortBehaviorFactory
    {
        ISortBehavior Create(SortOption option);
    }

    /// <summary>
    /// a sortBehavior factory that does not depend on DI, not so elegant 
    /// </summary>
    public class SortBehaviorFactory : ISortBehaviorFactory
    {
        public ISortBehavior Create(SortOption option)
        {
            switch (option)
            {
                case SortOption.AscendingLinqSorter: return new AscendingLinqSorter();

                case SortOption.DescendingLinqSorter: return new DescendingLinqSorter();

                default: return new NonSorter();
            }
        }
    }
}
