using System.Collections.Generic;

namespace REF.TextService.Domain.Contracts
{
    public interface ISortBehavior
    {
        string[] Sort(IEnumerable<string> words);
    }
}
