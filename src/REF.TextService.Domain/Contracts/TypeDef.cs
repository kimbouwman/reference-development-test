﻿namespace REF.TextService.Domain.Contracts
{
    public static class TypeDef
    {
        public const string WordsAndHypensDefinition = @"(\W|\w+\r\n)";
        public static readonly char[] HypensPattern = { '!', '-', ',', '\'' };
        public static readonly char[] PunctionsPattern = { '.' };
        public const string WordsPattern = @"\W+";
    }
}
