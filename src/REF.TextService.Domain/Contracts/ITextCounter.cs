
using REF.TextService.Contracts.DTOs.V1;

namespace REF.TextService.Domain.Contracts
{
    public interface ITextCounter
    {
        TextStatistics GetTotal(TextStatistics stats, string text);
    }
}
