﻿using System.Collections.Generic;
using System.ServiceModel;
using REF.TextService.Contracts.DTOs.V1;

namespace REF.TextService.Domain.Contracts
{
    [ServiceContract]
    public interface ITextService
    {
        [OperationContract]
        IEnumerable<string> Sort(string text, SortOption option);

        [OperationContract]
        TextStatistics GenerateStatistics(string text);
    }
}
