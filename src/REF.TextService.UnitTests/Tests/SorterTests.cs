﻿using Machine.Specifications;
using REF.TextService.Contracts.DTOs.V1;
using System.Linq;

namespace REF.TextService.UnitTests.Tests
{
    class SorterTests
    {
        class when_sorting_ascending_with_linq_sorter_then : SorterTestBase
        {
            It should_have_8_elements = () => result.Count().ShouldEqual(8);

            It should_have_equal_element_1 = () => result[0].ShouldEqual(expected[0]);
            It should_have_equal_element_2 = () => result[2].ShouldEqual(expected[2]);
            It should_have_equal_element_3 = () => result[3].ShouldEqual(expected[3]);
            It should_have_equal_element_4 = () => result[4].ShouldEqual(expected[4]);
            It should_have_equal_element_5 = () => result[5].ShouldEqual(expected[5]);
            It should_have_equal_element_6 = () => result[6].ShouldEqual(expected[6]);
            It should_have_equal_element_7 = () => result[7].ShouldEqual(expected[7]);

            Because of = () => result = sorter.Sort(input, SortOption.AscendingLinqSorter);

            Establish context = () => expected = new[] { "adipiscing", "amet", "consectetur", "dolor", "elit", "ipsum", "Lorem", "sit" };

            static string[] expected;
        }
    }

    class SorterTestBase : BootstrapperTestBase
    {
        It should_have_a_registrated_sorter = () => sorter.ShouldNotBeNull();

        Establish context = () => input = "Lorem ipsum dolor sit amet, consectetur \r\n adipiscing elit.";

        protected static string input;
        protected static string[] result;
    }
}
