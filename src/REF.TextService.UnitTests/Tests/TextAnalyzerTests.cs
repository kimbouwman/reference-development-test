﻿using Machine.Specifications;
using Ninject;
using REF.TextService.Contracts.DTOs.V1;
using System.Collections.Generic;
using System.Linq;
using REF.TextService.Domain.Contracts;
using REF.TextService.Domain.Processors;

namespace REF.TextService.UnitTests.Tests
{
    class TextAnalyzerTests
    {
        class when_analyzing_text_then : TextAnalyzerTestBase
        {
            It should_have_7_spaces = () => result.TotalSpaces.ShouldEqual(7);

            It should_have_8_words = () => result.TotalWords.ShouldEqual(8);

            It should_have_2_hypen = () => result.TotalHypens.ShouldEqual(2);

            Because of = () => result = analyzer.Analyse(input);
        }

    }

    class TextAnalyzerTestBase : BootstrapperTestBase
    {
        It should_have_counters = () => counters.Any().ShouldBeTrue();

        It should_have_a_registered_text_analyser = () => analyzer.ShouldNotBeNull();

        Establish context = () =>
        {
            counters = container.GetAll<ITextCounter>();
            analyzer = container.Get<ITextAnalyzer>();

            input = "Lorem ipsum dolor sit-amet, consectetur \r\n adipiscing elit.";

            result = new TextStatistics
            {
                TotalHypens = 2,
                TotalSpaces = 7,
                TotalWords = 8,
            };
        };


        protected static string input;
        protected static TextStatistics result;
        protected static ITextAnalyzer analyzer;
        protected static IEnumerable<ITextCounter> counters;
    }
}
