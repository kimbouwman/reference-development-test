﻿using Machine.Specifications;
using Ninject;
using REF.TextService.Domain.Processors;
using REF.TextService.Soap.Code;

namespace REF.TextService.UnitTests
{
    public class BootstrapperTestBase
    {
        It should_have_a_text_sorter = () => sorter.ShouldNotBeNull();

        Establish context = () =>
        {
            container = (IKernel)new Bootstrapper().Bootup(ObjectLifeStyle.Transient);

            sorter = container.Get<ITextSorter>();
        };

        protected static IKernel container;
        protected static ITextSorter sorter;
    }
}
