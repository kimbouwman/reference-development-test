This is an implementation of a dev test that I created. As the world is more moving to WebApi, this test becomes obsolete, but still can give you some insides into SOLID. 
 
It must show the candidate knowledge of the following technologies:
-	WCF/SOAP
-	LINQ
-	SOLID/Clean Code 
-	Knowledge/Usage of Dependency framework
-	Knowledge/Usage of Unit testing framework
-	Knowledge/Usage of Design Patterns 

The assessment: 
Create a WCF web service that can sort a piece of text. The service should be able to handle multiple sort behaviors. Implement at lease the standard ascending or descending ones.  In the future the service should be able to handle HeapSort, QuickSort or MergeSort. 

Secondly the service must support an analyze method that counts the hyphens, total spaces in text and words.

Create for extra points unit tests and/or test client for your service.
Think about service versioning, or how you would handle this, setup your code as such.

End of assessment definition 

Note about my solution
In this version I used Machine Specification as unit test framework, Ninject as IoC container and some other tools for code coverage, such as OpenCover and ReportGenerator. These are packages, so you can get from nuget.org.

Versioning is not really addressed in this solution. I am planning to upload a version pretty soon, using CQS.

For getting the reports, open the index.html file and select your report. 

Have fun!